# Dokerized Strapi 4.6.1 with Gitlab CI/CD configuration

# This is blank repo

## How to use

### Run on Local machine

- make sure to have .env file with proper values
- yarn (npm i)
- yarn develop (npm run develop)

### Build and run container

- docker build -t ${image-name} .
- docker run --name {container-name} -d -p ${port}:${port} ${image-name}

### Gitlab CI/CD

- Install and register gitlab runner on your server
- Create CI/CD variables in gitlab repo settings
- inspect setup_env.sh to much all variables from gitlab

## Postgres

This project configured to postgres. But as Postgres does not dockerize you can connect to any other DB.
When deploying by gitlab CI/CD make sure to configure Postgres settings (postgresql.conf and pg_hba.conf).
